package com.toryox.service.exception;

public class ToryoxServiceConfigException extends ToryoxServiceException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ToryoxServiceConfigException(String message, Throwable cause) {
		super(message, cause);
	}
}

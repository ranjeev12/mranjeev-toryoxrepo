package com.toryox.service.exception;

public class ToryoxServiceException extends RuntimeException{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ToryoxServiceException() {
		super();
	}

	public ToryoxServiceException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public ToryoxServiceException(String message, Throwable cause) {
		super(message, cause);
	}

	public ToryoxServiceException(String message) {
		super(message);
	}

	public ToryoxServiceException(Throwable cause) {
		super(cause);
	}

}

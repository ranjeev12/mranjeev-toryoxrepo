package com.toryox.platform;

import org.springframework.boot.SpringApplication;

public class PlatformApplication {

	public static void main(String[] args) {

		SpringApplication.run(PlatformApplication.class, args);
	}
}

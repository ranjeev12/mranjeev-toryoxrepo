package com.toryox.merchant;

import org.springframework.boot.SpringApplication;

public class MerchantApplication {

	public static void main(String[] args) {

		SpringApplication.run(MerchantApplication.class, args);
	}
}
